import React from 'react';

export default class TodoForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: ''
    }

  }
  doSubmit (e) {
    e.preventDefault();
    var task = this.state.value.trim();
    if (!task) {
      return;
    }
    this.props.onTaskSubmit(task);
    this.setState({
      value: ''
    });
    return;
  };

  changeHandle(e) {
    this.setState({
      value: e.target.value
    })
  };

  handleClickTodo(e) {
    this.props.sortTodo();
  };

  handleClickDone(e) {
    this.props.sortDone();
  };

  handleShowkAll(e) {
    this.props.showAll();
  };

  render() {

    return (
      <div className="commentForm vert-offset-top-2">
        <hr />
        <div className="clearfix">
          <form className="todoForm form-horizontal" onSubmit={(e) => this.doSubmit(e)}>
            <div className="form-group clearfix">
              <label htmlFor="task" className="col-md-2 control-label">Task</label>
              <div className="col-md-10">
                <input type="text" id="task" ref="input" value={this.state.value} onChange={(e) => this.changeHandle(e)} className="form-control" placeholder="What do you need to do?" />
              </div>
            </div>
            <div className="row">
              <div className="col-md-10 col-md-offset-2 text-right btn-wrapper">
                <input type="submit" value="Save Item" className="btn btn-primary pull-right" />
                <button className="btn btn-default" onClick={(e) => this.handleClickTodo(e)}>What to do?</button>
                <button className="btn btn-default" onClick={(e) => this.handleClickDone(e)}>What is done?</button>
                <button className="btn btn-default" onClick={(e) => this.handleShowkAll(e)}>Show all</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    )
  }

}
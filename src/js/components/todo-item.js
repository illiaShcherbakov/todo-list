import React from 'react';

export default class TodoItem extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      hide: '',
      showInput: false,
      value: this.props.task
    }
  }

  componentWillReceiveProps(nextProps) {
    if ( this.state.hide !== nextProps.hide ) {
      this.setState({
        hide: nextProps.hide,
      })
    }
  }

  removeNode (e) {
    e.preventDefault();
    this.props.removeNode(this.props.nodeId);
    return;
  };

  toggleComplete (e) {
    e.preventDefault();
    this.props.toggleComplete(this.props.nodeId);
    return;
  };

  changeHandle(e) {
    this.setState({
      value: e.target.value
    })
  };

  handleDoubleClick(e) {
    if ( !this.state.showInput ) {
      this.setState({
        showInput: true
      })
    }
  };

  doSubmit (e) {
    console.log('submit', this.props.edit);
    e.preventDefault();
    var task = this.state.value.trim();
    if (!task) {
      return;
    }
    this.props.edit(task, this.props.nodeId);
    this.setState({
      showInput: false
    });
    return;
  };

  render() {
    var classes = 'list-group-item clearfix';
    if (this.props.complete === true) {
      classes = classes + ' list-group-item-done';
    }
    if ( this.state.hide === true ) {
      classes = classes + ' hide';
    }


    return (
      <li className={classes} onDoubleClick={(e) => this.handleDoubleClick(e)}>
        { this.state.showInput ? (
            <form onSubmit={(e) => this.doSubmit(e)}>
              <input type="text" id="edit" ref="input" value={this.state.value} onChange={(e) => this.changeHandle(e)} className="form-control todoForm-edit" placeholder="What do you need to do?" />
              <input type="submit" value="Save Item" className="btn btn-primary pull-right" />
            </form>
          ) : (
          <div>
            <span >{this.props.task}</span>
            <div className="pull-right" role="group">
              <button type="button" className="btn btn-xs btn-success img-circle" onClick={(e) => this.toggleComplete(e)}>&#x2713;</button> <button type="button" className="btn btn-xs btn-danger img-circle" onClick={(e) => this.removeNode(e)}>&#x2715;</button>
            </div>
          </div>
          )
        }
      </li>
    )
  }
}
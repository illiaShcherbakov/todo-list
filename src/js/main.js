import $ from 'jquery';

import React from 'react';
import ReactDOM from 'react-dom';
import MyComponent from 'app.js';


ReactDOM.render(<MyComponent />, document.getElementById("app"));

import React from 'react';
import ReactDOM from 'react-dom';
import TodoList from './components/todo-list';
import TodoForm from './components/todo-form';


export default class MyComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [
        {
          "id":"00001",
          "task":"Task 1",
          "complete":false,
          "hide": false
        },
        {
          "id":"00002",
          "task":"Task 2",
          "complete":false,
          "hide": false
        },
        {
          "id":"00003",
          "task":"Task 3",
          "complete":false,
          "hide": false
        },
        {
          "id":"00004",
          "task":"Task 4",
          "complete":true,
          "hide": false
        }
      ]
    };

    this.handleNodeRemoval = this.handleNodeRemoval.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleToggleComplete = this.handleToggleComplete.bind(this);
    this.valueChangeHandle = this.valueChangeHandle.bind(this);
    this.handleSortTodo = this.handleSortTodo.bind(this);
    this.handleSortDone = this.handleSortDone.bind(this);
    this.handleShowAll = this.handleShowAll.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
  }

  generateId () {
    return Math.floor(Math.random()*90000) + 10000;
  };

  valueChangeHandle(e) {
    this.setState({
      value: e.target.value
    })
  }

  handleNodeRemoval (nodeId) {
    var data = this.state.data;
    data = data.filter(function (el) {
      return el.id !== nodeId;
    });
    this.setState({data});
    return;
  };

  handleSubmit (task) {
    var data = this.state.data;
    var id = this.generateId().toString();
    var complete = false;
    var hide = false;
    data = data.concat([{id, task, complete, hide}]);
    this.setState({data});
  };

  handleEdit(task, nodeId) {
    var data = this.state.data;
    var data = data.map(function (el) {
      if ( el.id === nodeId ) {
        el.task = task;
      }
      return el;
    });
    this.setState({data});
  }

  handleToggleComplete (nodeId) {
    var data = this.state.data;
    for (var i in data) {
      if (data[i].id == nodeId) {
        data[i].complete = data[i].complete === true ? false : true;
        break;
      }
    }
    this.setState({data});
    return;
  }

  handleSortTodo() {
    var data = this.state.data;
    var data = data.map(function (el) {
      if ( el.complete == false ) {
        el.hide = false;
      }
      if ( el.complete == true ) {
        el.hide = true;
      }
      return el;
    });
    this.setState({data});
  };

  handleSortDone() {
    var data = this.state.data;
    var data = data.map(function (el) {
      if ( el.complete == false ) {
        el.hide = true;
      }
      if ( el.complete == true ) {
        el.hide = false;
      }
      return el;
    });
    this.setState({data});
  };

  handleShowAll() {
    var data = this.state.data;
    var data = data.map(function (el) {
      if ( el.hide == true ) {
        el.hide = false;
      }
      return el;
    });
    this.setState({data});
  };

  render() {
    return (
      <div className="well todo-list-wrapper">
        <h1 className="vert-offset-top-0">To do:</h1>
        <TodoList data={this.state.data} removeNode={this.handleNodeRemoval} toggleComplete={this.handleToggleComplete} edit={this.handleEdit} />
        <TodoForm onTaskSubmit={this.handleSubmit} sortTodo={this.handleSortTodo} sortDone={this.handleSortDone} showAll={this.handleShowAll} />
      </div>
    )
  }
};